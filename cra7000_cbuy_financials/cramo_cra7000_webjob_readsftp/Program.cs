﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Azure.WebJobs;
using Cramo.Utils.Logging;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.IO;

namespace cramo_cra7000_webjob_readsftp
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage


        static void Main()
        {
            Trace.TraceInformation("Entering main. Initializing SFTP file fetch.");
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            // Start SFTP class
            SFTPClient client = new SFTPClient();
            client.initSFTPRead();

            Trace.TraceInformation("Exiting main. SFTP file fetch done.");
        }

        private static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Trace.TraceError(e.ExceptionObject.ToString());
        }

    }

    public class SFTPClient
    {

        private const string LOG_APP_NAME_READ = "CBuy-SFTP-ReadData-webjob";

        private string readPath = "";   // /files/out
        private string writePath = "";  // /files/in
        private string backupPath = ""; // /files/out/backup
        private string sftpServer = "";
        private string sftpUser = "";
        private string sftpPwd = "";
        private string sftpPort = "22";

        public string ReadPath
        {
            get { return readPath; }
            set { readPath = value; }
        }
        public string WritePath
        {
            get { return writePath; }
            set { writePath = value; }
        }
        public string BackupPath
        {
            get { return backupPath; }
            set { backupPath = value; }
        }
        public string SFTPServer
        {
            get { return sftpServer; }
            set { sftpServer = value; }
        }
        public string SFTPUser
        {
            get { return sftpUser; }
            set { sftpUser = value; }
        }
        public string SFTPPwd
        {
            get { return sftpPwd; }
            set { sftpPwd = value; }
        }
        public string SFTPPort
        {
            get { return sftpPort; }
            set { sftpPort = value; }
        }

        public void initSFTPRead()
        {

            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            string requestId = Guid.NewGuid().ToString();

            Logger logger = new Logger(LOG_APP_NAME_READ);
            logger.Log(Loglevel.Information, requestId, "5", "Initializing SFTP file fetch.", "");

            // Read settings for connection
            this.SFTPServer = System.Configuration.ConfigurationManager.AppSettings["SFTP.Host"].ToString();
            this.SFTPUser = System.Configuration.ConfigurationManager.AppSettings["SFTP.User"].ToString();
            this.SFTPPwd = System.Configuration.ConfigurationManager.AppSettings["SFTP.Password"].ToString();
            this.SFTPPort = System.Configuration.ConfigurationManager.AppSettings["SFTP.Port"].ToString();

            this.ReadPath = System.Configuration.ConfigurationManager.AppSettings["SFTP.ReadPath"].ToString();
            this.BackupPath = System.Configuration.ConfigurationManager.AppSettings["SFTP.BackupPath"].ToString();

            Trace.TraceInformation("SFTP config info - Host: " + this.SFTPServer);
            Trace.TraceInformation("SFTP config info - Port: " + this.SFTPPort);
            Trace.TraceInformation("SFTP config info - User: " + this.SFTPUser);
            Trace.TraceInformation("SFTP config info - Pwd : *********");

            Trace.TraceInformation("SFTP config info - Read from : " + this.ReadPath);
            Trace.TraceInformation("SFTP config info - Backup to : " + this.BackupPath);

            logger.Log(Loglevel.Information, requestId, "10", "Started SFTP poll from Basware", "");

            // Read files
            try
            {

                readFiles(logger, requestId);

            }
            catch (Exception e)
            {
                logger.Log(Loglevel.Error, requestId, "910", "A severe error occured!", e.ToString());
                Trace.TraceError("A severe error occured! -> " + e.ToString());

                throw;
            }

            logger.Log(Loglevel.Information, requestId, "50", "Finished SFTP poll from Basware", "");

        }

        public void readFiles(Logger logger, string requestId)
        {

            // Invoice file name for file fetch
            string filePrefix = "Transfer";
            string fileSuffix = ".xml";

            // Default 22
            int iPort = 22;
            try
            {
                iPort = Convert.ToInt32(this.SFTPPort);
            }
            catch (Exception e)
            {
                Trace.TraceInformation("Wrong port value: " + this.SFTPPort + ", defaulting to 22 for SFTP...");
                logger.Log(Loglevel.Information, requestId, "15", "Wrong port value: " + this.SFTPPort + ", defaulting to 22 for SFTP...", e.ToString());
            }

            // Logon
            try
            {
                using (var sftp = new SftpClient(this.SFTPServer, iPort, this.SFTPUser, this.SFTPPwd))
                {
                    Trace.TraceInformation("[SFTP] Connecting...");
                    sftp.Connect();
                    Trace.TraceInformation("[SFTP] Connected...");

                    // Adjust folders
                    if (!this.ReadPath.EndsWith("/")) { this.ReadPath = this.ReadPath + "/"; }
                    if (!this.BackupPath.EndsWith("/")) { this.BackupPath = this.BackupPath + "/"; }

                    Trace.TraceInformation("[SFTP] Read folder: " + this.ReadPath);
                    Trace.TraceInformation("[SFTP] Backup folder: " + this.BackupPath);

                    // Read files
                    var files = sftp.ListDirectory(this.ReadPath);
                    foreach (SftpFile f in files)
                    {

                        Trace.TraceInformation("[SFTP] File: " + f.Name);

                        String tmpFileName = f.Name;
                        String completePath = this.ReadPath + tmpFileName;
                        String destinationPath = this.BackupPath + tmpFileName + "." + getTimeStamp();

                        if (f.IsRegularFile &&
                            tmpFileName.ToLower().StartsWith(filePrefix.ToLower()) &&
                            tmpFileName.ToLower().EndsWith(fileSuffix.ToLower()))
                        {
                            Trace.TraceInformation("[SFTP] File matched filter! Should read and post file with name: " + completePath);
                            logger.Log(Loglevel.Information, requestId, "20", "Found file to read!", completePath);

                            int fileStatus = -1;
                            // 0 - NOT Sent before - continue
                            // 1 - Sent before - skip
                            // 2 - Error
                            // -1 - Not executed properly
                            try
                            {
                                if (!HasFileBeenSentBefore(f.Name))
                                {
                                    fileStatus = 0;
                                }
                                else
                                {
                                    fileStatus = 1;
                                }

                            }
                            catch (Exception)
                            {
                                // If error - try once more
                                Trace.TraceInformation("[SFTP] Could not find if file was sent before. Trying again! ");
                                logger.Log(Loglevel.Information, requestId, "900", "[SFTP] Could not find if file was sent before. Trying again! ", completePath);

                                try
                                {
                                    if (!HasFileBeenSentBefore(f.Name))
                                    {
                                        fileStatus = 0;
                                    }
                                    else
                                    {
                                        fileStatus = 1;
                                    }
                                }
                                catch (Exception ex2)
                                {
                                    Trace.TraceInformation("[SFTP] Could STILL not find if file was sent before. Keeping error");
                                    logger.Log(Loglevel.Information, requestId, "900", "[SFTP] Could STILL not find if file was sent before. Keeping error", ex2.ToString());

                                }
                            }

                            // Process file
                            Trace.TraceInformation("[SFTP] File status after [HasFileBeenSentBefore()]: " + fileStatus);
                            if (fileStatus == 0 || fileStatus == 2)
                            {

                                String content = sftp.ReadAllText(completePath);

                                Trace.TraceInformation("[SFTP] Read file contents...");

                                // HTTP Callback
                                String httpStatus = string.Empty;

                                try
                                {
                                    httpStatus = createHTTPPost(content);
                                    Trace.TraceInformation("[SFTP] Posted file contents. Status: " + httpStatus);

                                    logger.Log(Loglevel.Information, requestId, "30", "Posted contents OK to 360", "");
                                    Logfile(f.Name, "Successfully sent", string.Empty);

                                    // move to backup folder
                                    Trace.TraceInformation("[SFTP] Renaming file '" + completePath + "' to '" + destinationPath + "'.");
                                    sftp.RenameFile(completePath, destinationPath);
                                    Trace.TraceInformation("[SFTP] File renamed!");
                                    logger.Log(Loglevel.Information, requestId, "40", "File backed up to: " + destinationPath, "");

                                }
                                catch (WebException wex)
                                {
                                    Trace.TraceError("[SFTP] " + string.Format("Could not post content to 360, leaving file on SFTP! WebException: { 0}.", wex.Status.ToString()) );
                                    logger.Log(Loglevel.Error, requestId, "900", "Could not post content to 360, leaving file on SFTP!", string.Format("WebException: {0}.", wex.Status.ToString()));
                                    Logfile(f.Name, string.Format("Failed to send. WebException: {0}.", wex.Status.ToString()), wex.ToString());
                                }

                                catch (Exception ex)
                                {
                                    Trace.TraceError("[SFTP] Could not post content to 360, leaving file on SFTP! Unhandled exception: " + ex.ToString());
                                    logger.Log(Loglevel.Error, requestId, "900", "Could not post content to 360, leaving file on SFTP!", "Unhandled exception: " + ex.ToString());
                                    Logfile(f.Name, "Failed to send. Unhandled exception.", ex.ToString());
                                }

                            }
                            else
                            {
                                // File has been sent before - log and remove file.
                                Logfile(f.Name, "Ignored", "This file has already been processed.");

                                // rename remote
                                Trace.TraceInformation("[SFTP] Renaming file '" + completePath + "' to '" + destinationPath + "'.");
                                sftp.RenameFile(completePath, destinationPath);
                                Trace.TraceInformation("[SFTP] File renamed!");
                                logger.Log(Loglevel.Information, requestId, "40", "File backed up to: " + destinationPath, "");

                            }

                        }
                        else
                        {
                            Trace.TraceWarning("Skipping file/folder with name: " + completePath);
                        }
                    }

                    // Disconnect
                    Trace.TraceInformation("[SFTP] Disconnecting...");
                    if (sftp.IsConnected)
                    {
                        sftp.Disconnect();
                    }
                    Trace.TraceInformation("[SFTP] Disconnected!");

                }
            }
            catch (Renci.SshNet.Common.SshConnectionException sshce)
            {
                Trace.TraceError(sshce.ToString());
                throw;
            }
            catch (Exception) { throw; }

        }

        private String getTimeStamp()
        {
            DateTime d = DateTime.UtcNow;
            DateTime se = TimeZoneInfo.ConvertTimeFromUtc(d, TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time"));

            return se.ToString("yyyyMMddHHmmss");
        }

        public static bool HasFileBeenSentBefore(string fileName)
        {

            bool _hasFileBeenSentBefore = true;

            var _connectionStringEntry = ConfigurationManager.ConnectionStrings["cramo-esb-db"];

            string _connectionString = _connectionStringEntry.ConnectionString;

            using (SqlConnection _conn = new SqlConnection(_connectionString))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand _cmd = new SqlCommand("sp_GetFilelogRecord", _conn);
                _cmd.CommandTimeout = 60;

                // 2. set the command object so it knows to execute a stored procedure
                _cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                _cmd.Parameters.Add(new SqlParameter("@Filename", fileName));

                SqlParameter pFileHasBeenSentBefore = new SqlParameter();
                pFileHasBeenSentBefore.ParameterName = "@FileHasBeenSentBefore";
                pFileHasBeenSentBefore.DbType = DbType.Boolean;
                pFileHasBeenSentBefore.Direction = ParameterDirection.Output;
                _cmd.Parameters.Add(pFileHasBeenSentBefore);

                // execute the command
                _conn.Open();

                try
                {
                    _cmd.ExecuteNonQuery();
                    _hasFileBeenSentBefore = (bool)_cmd.Parameters["@FileHasBeenSentBefore"].Value;
                }
                catch (Exception e)
                {
                    Trace.TraceError("[SFTP] DB-Call: Could not execute DB-check! -> " + e.ToString());
                    throw;
                }
                finally
                {
                    _conn.Close();
                }

                Trace.TraceInformation("[SFTP] DB-Call: Has file been sent before: " + _hasFileBeenSentBefore.ToString());

                return _hasFileBeenSentBefore;

            }

        }

        public static void Logfile(string fileName, string status, string comment)
        {
            var _connectionStringEntry = ConfigurationManager.ConnectionStrings["cramo-esb-db"];

            string _connectionString = _connectionStringEntry.ConnectionString;

            using (SqlConnection _conn = new SqlConnection(_connectionString))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand _cmd = new SqlCommand("sp_InsertFilelogRecord", _conn);

                // 2. set the command object so it knows to execute a stored procedure
                _cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                _cmd.Parameters.Add(new SqlParameter("@Filename", fileName));
                _cmd.Parameters.Add(new SqlParameter("@Status", status));
                _cmd.Parameters.Add(new SqlParameter("@Comment", comment));

                // execute the command
                _conn.Open();

                try
                {
                    _cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    _conn.Close();
                }

            }
        }

        public String createHTTPPost(String xmlContent)
        {
            String responseBody = "";

            string baseAddress = System.Configuration.ConfigurationManager.AppSettings["360.BaseAddress"];
            string uri = System.Configuration.ConfigurationManager.AppSettings["360.Uri"];
            string subscriptionkey = System.Configuration.ConfigurationManager.AppSettings["360.SubscriptionKey"];

            String completeUrl = baseAddress + uri;

            HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(completeUrl);
            myHttpWebRequest.Method = "POST";
            myHttpWebRequest.Headers.Set("Ocp-Apim-Subscription-Key", subscriptionkey);

            byte[] data = Encoding.UTF8.GetBytes(xmlContent);

            myHttpWebRequest.ContentType = "text/xml";
            myHttpWebRequest.ContentLength = data.Length;

            Stream requestStream = myHttpWebRequest.GetRequestStream();
            requestStream.Write(data, 0, data.Length);
            requestStream.Close();

            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            HttpStatusCode sc = myHttpWebResponse.StatusCode;

            Stream responseStream = myHttpWebResponse.GetResponseStream();

            StreamReader myStreamReader = new StreamReader(responseStream, Encoding.UTF8);
            responseBody = myStreamReader.ReadToEnd();


            // Clean up
            myStreamReader.Close();
            responseStream.Close();
            myHttpWebResponse.Close();

            return sc.ToString();
        }

        private static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Trace.TraceError(e.ExceptionObject.ToString());
        }

    }
}
