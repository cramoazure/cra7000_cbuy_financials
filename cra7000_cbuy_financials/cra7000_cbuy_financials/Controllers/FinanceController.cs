﻿using cra7000_cbuy_financials.Utils;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Diagnostics;
using Cramo.Utils.Authentication.Filters;

namespace cra7000_cbuy_financials.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class FinanceController : ApiController
    {
        private const string LOG_APP_NAME_READ = "CBuy-ReadData";
        private const string LOG_APP_NAME_WRITE = "CBuy-TransferData";

        [HttpGet]
        public HttpResponseMessage Get(String listOnly = "false", String writeTestFile = "false")
        {
            HttpResponseMessage response = new HttpResponseMessage();
            FinancialFileHelper fh = new FinancialFileHelper();
            String requestId = Guid.NewGuid().ToString();

            Boolean bListOnly = true;
            Boolean bWriteTestFile = true;

            if (listOnly.ToLower().Equals("true")) { bListOnly = true; }
            if (writeTestFile.ToLower().Equals("true")) { bWriteTestFile = true; }

            String strReturn = "Searching for files on SFTP." + Environment.NewLine;
            strReturn += "Only list files? " + bListOnly + Environment.NewLine;

            Trace.TraceInformation("Started SFTP poll from Basware");

            // Read files
            try
            {
                SFTPClient sftp = new SFTPClient();
                Trace.TraceInformation("Initialized SFTP client...");

                // Write the testfile first
                if (bWriteTestFile)
                {
                    sftp.writeTestFile(fh.getInFolder(), requestId);
                }
                //String log = sftp.readFiles(fh.getInFolder(), fh.getBackupFolder(), bListOnly, requestId);
                sftp = null;
                Trace.TraceInformation("SFTP done!");

                //strReturn += log + Environment.NewLine;

            }
            catch (Exception e)
            {
                //DBHelper.log(LOG_APP_NAME_READ, requestId, "910", "A severe error occured!", e.ToString());
                Trace.TraceError("A severe error occured! " + e.ToString());

                response.Content = new StringContent(e.ToString());
                response.StatusCode = HttpStatusCode.InternalServerError;
                return response;
            }

            strReturn += "Done!" + Environment.NewLine;

            //DBHelper.log(LOG_APP_NAME_READ, requestId, "50", "Finished SFTP poll from Basware");
            Trace.TraceInformation("Finished SFTP poll from Basware.");
            response.Content = new StringContent(strReturn);
            response.StatusCode = HttpStatusCode.OK;

            return response;
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]String messageBody, [FromUri]String datatype, [FromUri]String rentaldb = "", [FromUri]String filename = "")
        {
 
            HttpResponseMessage response = new HttpResponseMessage();
            String requestId = Guid.NewGuid().ToString();

            Debug.WriteLine("messageBody: " + messageBody);
            DBHelper.log(LOG_APP_NAME_WRITE, requestId, "10", "Received SFTP Write request!", "Type: " + datatype + ", Market: " + rentaldb + ", Filename sent: " + filename);

            // Check if valid type
            FinancialFileHelper fh = new FinancialFileHelper();
            String uploadFilename = fh.translateTypeToFileName(datatype, rentaldb, requestId);
            Debug.WriteLine("uploadFilename: " + uploadFilename);
           

            // Wrong input - abort!
            if (messageBody == null || messageBody.Trim().Equals(""))
            {
                DBHelper.log(LOG_APP_NAME_WRITE, requestId, "900", "No body passed, can't write file", "Type: " + datatype + ", Market: " + rentaldb + ", Filename sent: " + filename);

                // Return immediately
                response.Content = new StringContent("<apiResponseMessage><message>You need to post a message to send!</message><type>error</type></apiResponseMessage>");
                response.StatusCode = HttpStatusCode.BadRequest;
                return response;
            }
            if (uploadFilename == null || uploadFilename.Trim().Equals(""))
            {
                DBHelper.log(LOG_APP_NAME_WRITE, requestId, "905", "Wrong type of datatype, unknown!", "Type: " + datatype + ", Market: " + rentaldb + ", Filename sent: " + filename);
                
                // Return immediately
                response.Content = new StringContent("<apiResponseMessage><message>Unknown kind of datatype, send a valid one.</message><type>error</type></apiResponseMessage>");
                response.StatusCode = HttpStatusCode.BadRequest;
                return response;
            }

            // Write file
            try {
                SFTPClient sftp = new SFTPClient();
                String path = fh.getOutFolder();
                Debug.WriteLine("path for outbound file: " + path);

                sftp.writeFile(path, uploadFilename, messageBody);
                DBHelper.log(LOG_APP_NAME_WRITE, requestId, "20", "File written OK to SFTP server as: " + uploadFilename, "Type: " + datatype + ", Market: " + rentaldb + ", Filename sent: " + filename);

            } catch (Exception e) {
                DBHelper.log(LOG_APP_NAME_WRITE, requestId, "910", "A severe error occured!", e.ToString());

                response.Content = new StringContent(e.ToString());
                response.StatusCode = HttpStatusCode.InternalServerError;
                return response;
            }

            // Ok
            response.Content = new StringContent("<apiResponseMessage><message>File uploaded successfully as: " + uploadFilename + "</message><type>info</type></apiResponseMessage>");
            response.StatusCode = HttpStatusCode.OK;
            return response;

        }
    }
}
