﻿using Renci.SshNet;
using Renci.SshNet.Sftp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace cra7000_cbuy_financials.Utils
{
    public class SFTPClient
    {

        private const string LOG_APP_NAME_READ = "CBuy-ReadData";

        public void writeFile(String path, String fileName, String content)
        {
            String sftpHost = System.Configuration.ConfigurationManager.AppSettings["SFTP.Host"];
            Debug.WriteLine("Host: " + sftpHost);
            String sftpPort = System.Configuration.ConfigurationManager.AppSettings["SFTP.Port"];
            Debug.WriteLine("Port: " + sftpPort);
            String sftpUser = System.Configuration.ConfigurationManager.AppSettings["SFTP.User"];
            Debug.WriteLine("User: " + sftpUser);
            String sftpPwd = System.Configuration.ConfigurationManager.AppSettings["SFTP.Password"];
            Debug.WriteLine("Pwd: " + sftpPwd);

            // Default 22
            int iPort = 22;
            try
            {
                iPort = Convert.ToInt32(sftpPort);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Wrong port value: " + sftpPort + ", defaulting to 22 for SFTP...");
            }

            // Logon
            try
            {
                using (var sftp = new SftpClient(sftpHost, iPort, sftpUser, sftpPwd))
                {
                    Debug.WriteLine("Connecting...");
                    sftp.Connect();
                    Debug.WriteLine("OK");

                    // Adjust path
                    if (!path.EndsWith("/"))
                    {
                        path = path + "/";
                    }

                    if (sftp.Exists(path))
                    {
                        Debug.WriteLine("Writing file!");
                        sftp.WriteAllText(path + fileName, content);
                        Debug.WriteLine("File written to: " + path + fileName);
                    }

                    Debug.WriteLine("Disconnecting...");
                    //sftp.Disconnect();
                    Debug.WriteLine("OK");

                }
            }
            catch (Renci.SshNet.Common.SshConnectionException e)
            {
                throw;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public string readFiles(String fromPath, String backupPath, bool bListOnly, String requestId)
        {
            String returnValue = "";
            FinancialFileHelper fh = new FinancialFileHelper();

            String sftpHost = System.Configuration.ConfigurationManager.AppSettings["SFTP.Host"];
            Debug.WriteLine("Host: " + sftpHost);
            String sftpPort = System.Configuration.ConfigurationManager.AppSettings["SFTP.Port"];
            Debug.WriteLine("Port: " + sftpPort);
            String sftpUser = System.Configuration.ConfigurationManager.AppSettings["SFTP.User"];
            Debug.WriteLine("User: " + sftpUser);
            String sftpPwd = System.Configuration.ConfigurationManager.AppSettings["SFTP.Password"];
            Debug.WriteLine("Pwd: " + sftpPwd);

            // Invoice file name
            String filePrefix = "Transfer";
            String fileSuffix = ".xml";

            // Default 22
            int iPort = 22;
            try
            {
                iPort = Convert.ToInt32(sftpPort);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Wrong port value: " + sftpPort + ", defaulting to 22 for SFTP...");
            }

            // Logon
            try
            {
                using (var sftp = new SftpClient(sftpHost, iPort, sftpUser, sftpPwd))
                {
                    Debug.WriteLine("Connecting...");
                    sftp.Connect();
                    Debug.WriteLine("OK");

                    // Adjust folders
                    if (!fromPath.EndsWith("/")) { fromPath = fromPath + "/"; }
                    if (!backupPath.EndsWith("/")) { backupPath = backupPath + "/"; }

                    returnValue += "In folder: " + fromPath + Environment.NewLine;
                    returnValue += "Backup folder: " + backupPath + Environment.NewLine;

                    // Read files
                    var files = sftp.ListDirectory(fromPath);
                    foreach (SftpFile f in files)
                    {

                        Debug.WriteLine(f.Name);

                        String tmpFileName = f.Name;
                        String completePath = fromPath + tmpFileName;
                        String destinationPath = backupPath + tmpFileName + "." + getTimeStamp();

                        if (f.IsRegularFile &&
                            tmpFileName.ToLower().StartsWith(filePrefix.ToLower()) &&
                            tmpFileName.ToLower().EndsWith(fileSuffix.ToLower()))
                        {
                            Debug.WriteLine("FileToRead: " + completePath);
                            returnValue += "File matched filter! Should read and post file with name: " + completePath + Environment.NewLine;
                            DBHelper.log(LOG_APP_NAME_READ, requestId, "20", "Found file to read!", completePath);

                            if (!bListOnly)
                            {

                                if (!DBHelper.HasFileBeenSentBefore(f.Name))
                                {

                                    String content = sftp.ReadAllText(completePath);

                                    Debug.WriteLine("Read file, contents: " + content);

                                    // HTTP Callback
                                    String httpStatus = string.Empty;

                                    try
                                    {
                                        httpStatus = fh.createHTTPPost(content);
                                        returnValue += "Posted file contents. Status: " + httpStatus + Environment.NewLine;

                                        DBHelper.log(LOG_APP_NAME_READ, requestId, "30", "Posted contents OK to 360");
                                        DBHelper.Logfile(f.Name, "Successfully sent", string.Empty);

                                        // move to backup folder

                                        Debug.WriteLine("Renaming file '" + completePath + "' to '" + destinationPath + "'.");
                                        sftp.RenameFile(completePath, destinationPath);
                                        DBHelper.log(LOG_APP_NAME_READ, requestId, "40", "File backed up to: " + destinationPath);

                                    }
                                    catch (WebException wex)
                                    {
                                        DBHelper.log(LOG_APP_NAME_READ, requestId, "900", string.Format("Could not post content to 360, leaving file on SFTP! WebException: {0}.", wex.Status.ToString()));
                                        DBHelper.Logfile(f.Name, string.Format("Failed to send. WebException: {0}.", wex.Status.ToString()), wex.ToString());
                                        returnValue += "Failed to post file contents.";
                                    }

                                    catch (Exception ex)
                                    {
                                        DBHelper.log(LOG_APP_NAME_READ, requestId, "900", "Could not post content to 360, leaving file on SFTP! Unhandled exception.", ex.ToString());
                                        DBHelper.Logfile(f.Name, "Failed to send. Unhandled exception.", ex.ToString());
                                        returnValue += "Failed to post file contents.";
                                    }

                                }
                                else
                                {
                                    // File has been sent before - log and remove file.
                                    DBHelper.Logfile(f.Name, "Ignored", "This file has already been processed.");

                                    // rename remote
                                    Debug.WriteLine("Renaming file '" + completePath + "' to '" + destinationPath + "'.");
                                    sftp.RenameFile(completePath, destinationPath);
                                    DBHelper.log(LOG_APP_NAME_READ, requestId, "40", "File backed up to: " + destinationPath);
                                }
                            }
                        }
                        else
                        {
                            Debug.WriteLine("Skipping file/folder with name: " + completePath);
                            returnValue += "Skipped file/folder with name: " + completePath + Environment.NewLine;
                        }
                    }

                    // Disconnect
                    Debug.WriteLine("Disconnecting...");
                    if (sftp.IsConnected)
                    {
                        sftp.Disconnect();
                    }
                    returnValue += "Disconnected!" + Environment.NewLine;
                    Debug.WriteLine("OK");

                }
            }
            catch (Renci.SshNet.Common.SshConnectionException sshce)
            {
                //Debug.WriteLine(e.ToString());
            }
            catch (Exception e) { throw; }

            return returnValue;
        }

        public void writeTestFile(String path, String requestId)
        {
            String sftpHost = System.Configuration.ConfigurationManager.AppSettings["SFTP.Host"];
            String sftpPort = System.Configuration.ConfigurationManager.AppSettings["SFTP.Port"];
            String sftpUser = System.Configuration.ConfigurationManager.AppSettings["SFTP.User"];
            String sftpPwd = System.Configuration.ConfigurationManager.AppSettings["SFTP.Password"];

            // Invoice file name
            String filePrefix = "Transfer";
            String fileSuffix = ".xml";

            String fileName = filePrefix + "_TEST_" + requestId + fileSuffix;

            // Read the content of the test file
            String invoiceFile = System.Web.HttpContext.Current.Server.MapPath("~") + "\\Resources\\ProcessInvoiceIn_FJ_1_TEST.xml";
            Trace.TraceInformation("InFile: " + invoiceFile);

            String content = "";
            using (StreamReader streamReader = new StreamReader(invoiceFile, Encoding.UTF8))
            {
                content = streamReader.ReadToEnd();
            }

            // Write the file
            // Default 22
            int iPort = 22;
            try
            {
                iPort = Convert.ToInt32(sftpPort);
            }
            catch (Exception e)
            {
                Trace.TraceInformation("Wrong port value: " + sftpPort + ", defaulting to 22 for SFTP...");
            }

            // Logon
            try
            {
                using (var sftp = new SftpClient(sftpHost, iPort, sftpUser, sftpPwd))
                {
                    Trace.TraceInformation("Connecting...");
                    sftp.Connect();
                    Trace.TraceInformation("OK");

                    // Adjust path
                    if (!path.EndsWith("/"))
                    {
                        path = path + "/";
                    }

                    if (sftp.Exists(path))
                    {
                        Trace.TraceInformation("Writing test file!");
                        sftp.WriteAllText(path + fileName, content);
                        Trace.TraceInformation("Wrote a testfile to the SFTP server! -> " + path + fileName);
                        //DBHelper.log(LOG_APP_NAME_READ, requestId, "05", "Wrote a testfile to the SFTP server!", path + fileName);
                    }

                    Trace.TraceInformation("Disconnecting...");
                    //sftp.Disconnect();
                    Trace.TraceInformation("OK");

                }
            }
            catch (Renci.SshNet.Common.SshConnectionException e)
            {

            }
            catch (Exception e)
            {
                throw;
            }

        }

        private String getTimeStamp()
        {
            DateTime d = DateTime.UtcNow;
            DateTime se = TimeZoneInfo.ConvertTimeFromUtc(d, TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time"));

            return se.ToString("yyyyMMddHHmmss");
        }
    }
}