﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;

namespace cra7000_cbuy_financials.Utils
{
    public class DBHelper
    {
        public static void log(String applicationName, String requestKey, String stepId, String logText, String logMessage = "")
        {

            var _connectionStringEntry = ConfigurationManager.ConnectionStrings["cramo-esb-db"];

            string _connectionString = _connectionStringEntry.ConnectionString;

            using (SqlConnection _conn = new SqlConnection(_connectionString))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand _cmd = new SqlCommand("sp_InsertLogRecord", _conn);

                // 2. set the command object so it knows to execute a stored procedure
                _cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                _cmd.Parameters.Add(new SqlParameter("@LogApplication", applicationName));
                _cmd.Parameters.Add(new SqlParameter("@LogKey", requestKey));
                _cmd.Parameters.Add(new SqlParameter("@LogStep", stepId));
                _cmd.Parameters.Add(new SqlParameter("@LogText", logText));
                _cmd.Parameters.Add(new SqlParameter("@LogMessage", logMessage));

                // execute the command
                _conn.Open();

                try
                {
                    _cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    _conn.Close();
                }

            }
        }

        public static void Logfile(string fileName, string status, string comment)
        {
            var _connectionStringEntry = ConfigurationManager.ConnectionStrings["cramo-esb-db"];

            string _connectionString = _connectionStringEntry.ConnectionString;

            using (SqlConnection _conn = new SqlConnection(_connectionString))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand _cmd = new SqlCommand("sp_InsertFilelogRecord", _conn);

                // 2. set the command object so it knows to execute a stored procedure
                _cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                _cmd.Parameters.Add(new SqlParameter("@Filename", fileName));
                _cmd.Parameters.Add(new SqlParameter("@Status", status));
                _cmd.Parameters.Add(new SqlParameter("@Comment", comment));

                // execute the command
                _conn.Open();

                try
                {
                    _cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    _conn.Close();
                }

            }
        
        }

        public static bool HasFileBeenSentBefore(string fileName)
        {

            bool _hasFileBeenSentBefore = true;

            var _connectionStringEntry = ConfigurationManager.ConnectionStrings["cramo-esb-db"];

            string _connectionString = _connectionStringEntry.ConnectionString;

            using (SqlConnection _conn = new SqlConnection(_connectionString))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand _cmd = new SqlCommand("sp_GetFilelogRecord", _conn);

                // 2. set the command object so it knows to execute a stored procedure
                _cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                _cmd.Parameters.Add(new SqlParameter("@Filename", fileName));

                SqlParameter pFileHasBeenSentBefore = new SqlParameter();
                pFileHasBeenSentBefore.ParameterName = "@FileHasBeenSentBefore";
                pFileHasBeenSentBefore.DbType = DbType.Boolean;
                pFileHasBeenSentBefore.Direction = ParameterDirection.Output;
                _cmd.Parameters.Add(pFileHasBeenSentBefore);            

                // execute the command
                _conn.Open();

                try
                {
                    _cmd.ExecuteNonQuery();
                    _hasFileBeenSentBefore = (bool)_cmd.Parameters["@FileHasBeenSentBefore"].Value;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    _conn.Close();
                }

                Debug.WriteLine("Has file been sent before: " + _hasFileBeenSentBefore.ToString());

                return _hasFileBeenSentBefore;

            }
         
        }
       
    }
}