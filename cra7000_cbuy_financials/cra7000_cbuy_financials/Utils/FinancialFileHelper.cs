﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Text;
using System.IO;

namespace cra7000_cbuy_financials.Utils
{
    public class FinancialFileHelper
    {
        private String inFolder = "/files/in";
        private String outFolder = "/files/out";
        private String backupFolder = "/files/out/backup";

        public String translateTypeToFileName(String type, String market, String UUID)
        {
            /*
                suppliers Suppliers$Market$_$Timestamp$_$RequestId$.xml
                exchangerates   ExchangeRates$Market$_$Timestamp$_$RequestId$.xml
                paymentterms    PaymentTerms$Market$_$Timestamp$_$RequestId$.xml
                accounts    Accounts$Market$_$Timestamp$_$RequestId$.xml
                transferresponse    TransferResponse$Market$_$Timestamp$_$RequestId$.xml
                paymentresponse PaymentResponse$Market$_$Timestamp$_$RequestId$.xml
            */

            String returnValue = null;
            Debug.WriteLine("type: " + type);

            type = type.ToLower();
            market = market.ToUpper();

            if (type.Equals("supplier") || type.Equals("suppliers"))
            {
                returnValue = "Suppliers_$Market$_$Timestamp$_$RequestId$.xml";
            }
            else if (type.Equals("exchangerates")) {
                returnValue = "ExchangeRates_$Market$_$Timestamp$_$RequestId$.xml";
            }
            else if (type.Equals("paymentterms"))
            {
                returnValue = "PaymentTerms_$Market$_$Timestamp$_$RequestId$.xml";
            }
            else if (type.Equals("account") || type.Equals("accounts"))
            {
                returnValue = "Accounts_$Market$_$Timestamp$_$RequestId$.xml";
            }
            else if (type.Equals("transferresponse"))
            {
                returnValue = "TransferResponse_$Market$_$Timestamp$_$RequestId$.xml";
            }
            else if (type.Equals("paymentresponse"))
            {
                returnValue = "PaymentResponse_$Market$_$Timestamp$_$RequestId$.xml";
            }
            else if (type.Equals("purchaseorder") || type.Equals("purchaseorders"))
            {
                returnValue = "PurchaseOrder_$Market$_$Timestamp$_$RequestId$.xml";
            } else
            {
                Debug.WriteLine("type gives no match...");
                returnValue = null;
                return returnValue;
            }

            // Replace Strings
            Debug.WriteLine("returnValue 1) " + returnValue);
            returnValue = returnValue.Replace("$RequestId$", UUID);
            returnValue = returnValue.Replace("$Timestamp$", getTimeStamp());
            returnValue = returnValue.Replace("$Market$", market);
            Debug.WriteLine("returnValue 2) " + returnValue);

            return returnValue;

        }

        public string getOutFolder()
        {
            // The outfolder for the integration is the input on the SFTP-server
            return this.inFolder;
        }

        public string getInFolder()
        {
            // The in-folder for the integration is the output on the SFTP-server
            return this.outFolder;
        }
        public string getBackupFolder()
        {
            return this.backupFolder;
        }

        private String getTimeStamp()
        {
            DateTime d = DateTime.UtcNow;
            DateTime se = TimeZoneInfo.ConvertTimeFromUtc(d, TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time"));

            return se.ToString("yyyyMMddHHmmss");
        }


        public String createHTTPPost(String xmlContent)
        {
            String responseBody = "";

            string baseAddress = System.Configuration.ConfigurationManager.AppSettings["360.BaseAddress"];
            string uri = System.Configuration.ConfigurationManager.AppSettings["360.Uri"];
            string subscriptionkey = System.Configuration.ConfigurationManager.AppSettings["360.SubscriptionKey"];

            String completeUrl = baseAddress + uri;

            HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(completeUrl);
            myHttpWebRequest.Method = "POST";
            myHttpWebRequest.Headers.Set("Ocp-Apim-Subscription-Key", subscriptionkey);

            byte[] data = Encoding.UTF8.GetBytes(xmlContent);

            myHttpWebRequest.ContentType = "text/xml";
            myHttpWebRequest.ContentLength = data.Length;

            Stream requestStream = myHttpWebRequest.GetRequestStream();
            requestStream.Write(data, 0, data.Length);
            requestStream.Close();

            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            HttpStatusCode sc = myHttpWebResponse.StatusCode;

            Stream responseStream = myHttpWebResponse.GetResponseStream();

            StreamReader myStreamReader = new StreamReader(responseStream, Encoding.UTF8);
            responseBody = myStreamReader.ReadToEnd();


            // Clean up
            myStreamReader.Close();
            responseStream.Close();
            myHttpWebResponse.Close();

            return sc.ToString(); 
        }

    }
}