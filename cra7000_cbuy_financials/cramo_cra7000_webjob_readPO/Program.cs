﻿using System.Configuration;
using Microsoft.Azure.WebJobs;
using Cramo.Utils.Logging;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.IO;
using System.Xml.Linq;
using System;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;

namespace cramo_cra7000_webjob_readPO
{

    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        static string _nameIdentifier = "Read Purchase Order";

        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            Trace.TraceInformation("[" + _nameIdentifier + "] Entering main. Initializing SFTP file fetch. ");
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            // Start SFTP class
            SFTPClient client = new SFTPClient();
            client.initSFTPRead();

            Trace.TraceInformation("[" + _nameIdentifier + "] Exiting main. SFTP file fetch done. ");
            return;
        }

        private static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Trace.TraceError("[" + _nameIdentifier + "] " + e.ExceptionObject.ToString());
        }

    }

    public class SFTPClient
    {

        private const string LOG_APP_NAME_READ = "CBuy-SFTP-ReadData-webjob";
        private const string _nameIdentifier = "Read Purchase Order";

        private string readPath = "";   // /files/out
        private string writePath = "";  // /files/in
        private string backupPath = ""; // /files/out/backup
        private string sftpServer = "";
        private string sftpUser = "";
        private string sftpPwd = "";
        private string sftpPort = "22";
        private string sftpFileMask = "";

        public string ReadPath
        {
            get { return readPath; }
            set { readPath = value; }
        }
        public string WritePath
        {
            get { return writePath; }
            set { writePath = value; }
        }
        public string BackupPath
        {
            get { return backupPath; }
            set { backupPath = value; }
        }
        public string SFTPServer
        {
            get { return sftpServer; }
            set { sftpServer = value; }
        }
        public string SFTPUser
        {
            get { return sftpUser; }
            set { sftpUser = value; }
        }
        public string SFTPPwd
        {
            get { return sftpPwd; }
            set { sftpPwd = value; }
        }
        public string SFTPPort
        {
            get { return sftpPort; }
            set { sftpPort = value; }
        }

        public string SFTPFileMask
        {
            get { return sftpFileMask; }
            set { sftpFileMask = value; }
        }

        public void initSFTPRead()
        {

            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            string requestId = Guid.NewGuid().ToString();

            Logger logger = new Logger(LOG_APP_NAME_READ);
            logger.Log(Loglevel.Information, requestId, "5", "[ReadPO] Initializing SFTP file fetch.", "");

            // Read settings for connection
            this.SFTPServer = System.Configuration.ConfigurationManager.AppSettings["SFTP.Host"].ToString();
            this.SFTPUser = System.Configuration.ConfigurationManager.AppSettings["SFTP.User"].ToString();
            this.SFTPPwd = System.Configuration.ConfigurationManager.AppSettings["SFTP.Password"].ToString();
            this.SFTPPort = System.Configuration.ConfigurationManager.AppSettings["SFTP.Port"].ToString();

            this.ReadPath = System.Configuration.ConfigurationManager.AppSettings["SFTP.ReadPath"].ToString();
            this.BackupPath = System.Configuration.ConfigurationManager.AppSettings["SFTP.BackupPath"].ToString();
            this.SFTPFileMask = System.Configuration.ConfigurationManager.AppSettings["SFTP.PO.FileMask"].ToString();

            Trace.TraceInformation("SFTP config info - Host: " + this.SFTPServer);
            Trace.TraceInformation("SFTP config info - Port: " + this.SFTPPort);
            Trace.TraceInformation("SFTP config info - User: " + this.SFTPUser);
            Trace.TraceInformation("SFTP config info - Pwd : *********");

            Trace.TraceInformation("SFTP config info - Read from    : " + this.ReadPath);
            Trace.TraceInformation("SFTP config info - Backup to    : " + this.BackupPath);
            Trace.TraceInformation("SFTP config info - File mask PO : " + this.SFTPFileMask);

            logger.Log(Loglevel.Information, requestId, "10", "[ReadPO] Started SFTP poll from Basware", "");

            // Read files
            try
            {

                readFiles(logger, requestId);

            }
            catch (Exception e)
            {
                logger.Log(Loglevel.Error, requestId, "910", "[ReadPO] A severe error occured!", e.ToString());
                Trace.TraceError("[" + _nameIdentifier + "] A severe error occured! -> " + e.ToString());

                throw;
            }

            logger.Log(Loglevel.Information, requestId, "50", "[ReadPO] Finished SFTP poll from Basware", "");

        }

        public Boolean Fits(string sFileName, string sFileMask)
        {
            //String convertedMask = "^" + Regex.Escape(sFileMask).Replace("\\*", ".*").Replace("\\?", ".") + "$";
            Regex regexMask = new Regex(sFileMask, RegexOptions.IgnoreCase);
            Trace.TraceInformation("[" + _nameIdentifier + "] FileFilter: " + sFileMask);
            Trace.TraceInformation("[" + _nameIdentifier + "] File name match: " + regexMask.IsMatch(sFileName));
            return regexMask.IsMatch(sFileName);
        }

        public void readFiles(Logger logger, string requestId)
        {

            // Invoice file name for file fetch
            //string filePrefix = "Transfer";
            //string fileSuffix = ".xml";

            TelemetryClient telemetry;
            TraceTelemetry tt;
            ExceptionTelemetry ttError;

            // Default 22
            int iPort = 22;
            try
            {
                iPort = Convert.ToInt32(this.SFTPPort);
            }
            catch (Exception e)
            {
                Trace.TraceInformation("[" + _nameIdentifier + "] Wrong port value: " + this.SFTPPort + ", defaulting to 22 for SFTP...");
                logger.Log(Loglevel.Information, requestId, "15", "[ReadPO] Wrong port value: " + this.SFTPPort + ", defaulting to 22 for SFTP...", e.ToString());
            }

            // Logon
            try
            {
                using (var sftp = new SftpClient(this.SFTPServer, iPort, this.SFTPUser, this.SFTPPwd))
                {
                    telemetry = new TelemetryClient();
                    telemetry.TrackTrace("Purchase order file GET start", SeverityLevel.Information);

                    Trace.TraceInformation("[" + _nameIdentifier + "] Connecting...");
                    sftp.Connect();
                    Trace.TraceInformation("[" + _nameIdentifier + "] Connected...");

                    // Adjust folders
                    if (!this.ReadPath.EndsWith("/")) { this.ReadPath = this.ReadPath + "/"; }
                    if (!this.BackupPath.EndsWith("/")) { this.BackupPath = this.BackupPath + "/"; }

                    Trace.TraceInformation("[" + _nameIdentifier + "] Read folder: " + this.ReadPath);
                    Trace.TraceInformation("[" + _nameIdentifier + "] Backup folder: " + this.BackupPath);

                    // Read files
                    var files = sftp.ListDirectory(this.ReadPath);
                    foreach (SftpFile f in files)
                    {

                        Trace.TraceInformation("[" + _nameIdentifier + "] File: " + f.Name);
                        tt = new TraceTelemetry("Trace SFTP Read from Basware.", SeverityLevel.Information);
                        

                        String tmpFileName = f.Name;
                        String completePath = this.ReadPath + tmpFileName;
                        String destinationPath = this.BackupPath + tmpFileName + "." + getTimeStamp();

                        tt.Properties.Add("Filename", completePath);

                        if (f.IsRegularFile &&
                            Fits(f.Name, this.SFTPFileMask))
                        {
                            Trace.TraceInformation("[" + _nameIdentifier + "] File matched filter! Should read and post file with name: " + completePath);
                            logger.Log(Loglevel.Information, requestId, "20", "[ReadPO] Found file to read!", completePath);
                            tt.Properties.Add("Passed file filter", "Ok");

                            int fileStatus = -1;
                            // 0 - NOT Sent before - continue
                            // 1 - Sent before - skip
                            // 2 - Error
                            // -1 - Not executed properly
                            try
                            {
                                if (!HasFileBeenSentBefore(f.Name))
                                {
                                    fileStatus = 0;
                                }
                                else
                                {
                                    fileStatus = 1;
                                }

                            }
                            catch (Exception)
                            {
                                // If error - try once more
                                Trace.TraceInformation("[" + _nameIdentifier + "] Could not find if file was sent before. Trying again! ");
                                logger.Log(Loglevel.Information, requestId, "900", "[ReadPO] Could not find if file was sent before. Trying again! ", completePath);

                                try
                                {
                                    if (!HasFileBeenSentBefore(f.Name))
                                    {
                                        fileStatus = 0;
                                    }
                                    else
                                    {
                                        fileStatus = 1;
                                    }
                                }
                                catch (Exception ex2)
                                {
                                    Trace.TraceInformation("[" + _nameIdentifier + "] Could STILL not find if file was sent before. Keeping error");
                                    logger.Log(Loglevel.Information, requestId, "900", "[ReapPO] Could STILL not find if file was sent before. Keeping error", ex2.ToString());

                                }
                            }

                            // Process file
                            Trace.TraceInformation("[" + _nameIdentifier + "] File status after [HasFileBeenSentBefore()]: " + fileStatus);
                            if (fileStatus == 0 || fileStatus == 2)
                            {

                                String content = sftp.ReadAllText(completePath);
                                Trace.TraceInformation("[" + _nameIdentifier + "] Read file contents...");
                                tt.Properties.Add("File processing", "Ok to process");

                                // Find the unique ID in the file
                                string PONumber = "";
                                Dictionary<string, string> POinfo = GetPurchaseOrderInfo(content);
                                POinfo.TryGetValue("OrderId", out PONumber);
                                Trace.TraceInformation("[" + _nameIdentifier + "] PO Number (id): " + PONumber);

                                tt.Properties.Add("PO number", PONumber);

                                // Write to queue
                                String httpStatus = string.Empty;

                                try
                                {
                                    httpStatus = createHTTPTopicPost(content);
                                    Trace.TraceInformation("[" + _nameIdentifier + "] Posted file contents to queue. Status: " + httpStatus);

                                    if (httpStatus.Trim().Equals("OK") ) { 
                                        logger.Log(Loglevel.Information, requestId, "30", "[ReadPO] Posted contents OK to topic 'purchaseorders'.", "");
                                        Logfile(f.Name, "Successfully sent", "Id: " + PONumber);

                                        // move to backup folder
                                        Trace.TraceInformation("[" + _nameIdentifier + "] Renaming file '" + completePath + "' to '" + destinationPath + "'.");
                                        sftp.RenameFile(completePath, destinationPath);
                                        Trace.TraceInformation("[" + _nameIdentifier + "] File renamed!");
                                        logger.Log(Loglevel.Information, requestId, "40", "[ReadPO] File backed up to: " + destinationPath, "");

                                        tt.Properties.Add("HTTP status", "200 - OK");
                                        tt.Properties.Add("File backup", destinationPath);
                                    } else
                                    {
                                        // Error
                                        throw new Exception("Error storing message on topic 'purchaseorders'. Status: " + httpStatus); 
                                    }

                                }
                                catch (WebException wex)
                                {
                                    Trace.TraceError("[" + _nameIdentifier + "] " + string.Format("Could not post content to topic, leaving file on SFTP! WebException: { 0}.", wex.Status.ToString()));
                                    logger.Log(Loglevel.Error, requestId, "900", "[ReadPO] Could not post content to topic, leaving file on SFTP!", string.Format("WebException: {0}.", wex.Status.ToString()));
                                    Logfile(f.Name, string.Format("Failed to send to topic. WebException: {0}.", wex.Status.ToString()), wex.ToString());

                                    ttError = new ExceptionTelemetry(wex);
                                    ttError.Properties.Add("Component", "cramo_cra7000_webjob_readPO::SFTPClient");
                                    ttError.Properties.Add("Method", "readFiles");
                                    ttError.Properties.Add("Cause", "Could not post content to topic, leaving file on SFTP! WebException: " + wex.Status.ToString());

                                    telemetry.TrackException(ttError);

                                }

                                catch (Exception ex)
                                {
                                    Trace.TraceError("[" + _nameIdentifier + "] Could not post content to topic, leaving file on SFTP! Unhandled exception: " + ex.ToString());
                                    logger.Log(Loglevel.Error, requestId, "900", "[ReadPO] Could not post content to topic, leaving file on SFTP!", "Unhandled exception: " + ex.ToString());
                                    Logfile(f.Name, "Failed to send to topic. Unhandled exception. ", ex.ToString());

                                    ttError = new ExceptionTelemetry(ex);
                                    ttError.Properties.Add("Component", "cramo_cra7000_webjob_readPO::SFTPClient");
                                    ttError.Properties.Add("Method", "readFiles");
                                    ttError.Properties.Add("Cause", "Failed to send to topic. Unhandled exception: " + ex.ToString());

                                    telemetry.TrackException(ttError);
                                }


                            }
                            else
                            {
                                // File has been sent before - log and remove file.
                                Logfile(f.Name, "Ignored", "This file has already been processed.");
                                tt.Properties.Add("File processing", "Processed before, skipped");

                                // rename remote
                                Trace.TraceInformation("[" + _nameIdentifier + "] Renaming file '" + completePath + "' to '" + destinationPath + "'.");
                                sftp.RenameFile(completePath, destinationPath);
                                Trace.TraceInformation("[" + _nameIdentifier + "] File renamed!");
                                logger.Log(Loglevel.Information, requestId, "40", "[ReadPO] File backed up to: " + destinationPath, "");

                                tt.Properties.Add("File backup", destinationPath);

                            }

                        }
                        else
                        {
                            Trace.TraceWarning("[" + _nameIdentifier + "] Skipping file/folder with name: " + completePath);
                        }

                        telemetry.TrackTrace(tt);
                    }



                    telemetry.TrackTrace("Purchase order file GET done", SeverityLevel.Information);

                    // Disconnect
                    Trace.TraceInformation("[" + _nameIdentifier + "] Disconnecting...");
                    if (sftp.IsConnected)
                    {
                        sftp.Disconnect();
                    }
                    Trace.TraceInformation("[" + _nameIdentifier + "] Disconnected!");

                }
            }
            catch (Renci.SshNet.Common.SshConnectionException sshce)
            {
                Trace.TraceError(sshce.ToString());
                throw;
            }
            catch (Exception) { throw; }

        }

        private String getTimeStamp()
        {
            DateTime d = DateTime.UtcNow;
            DateTime se = TimeZoneInfo.ConvertTimeFromUtc(d, TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time"));

            return se.ToString("yyyyMMddHHmmss");
        }

        public static bool HasFileBeenSentBefore(string fileName)
        {

            bool _hasFileBeenSentBefore = true;

            var _connectionStringEntry = ConfigurationManager.ConnectionStrings["cramo-esb-db"];

            string _connectionString = _connectionStringEntry.ConnectionString;

            using (SqlConnection _conn = new SqlConnection(_connectionString))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand _cmd = new SqlCommand("sp_GetFilelogRecord", _conn);
                _cmd.CommandTimeout = 60;

                // 2. set the command object so it knows to execute a stored procedure
                _cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                _cmd.Parameters.Add(new SqlParameter("@Filename", fileName));

                SqlParameter pFileHasBeenSentBefore = new SqlParameter();
                pFileHasBeenSentBefore.ParameterName = "@FileHasBeenSentBefore";
                pFileHasBeenSentBefore.DbType = DbType.Boolean;
                pFileHasBeenSentBefore.Direction = ParameterDirection.Output;
                _cmd.Parameters.Add(pFileHasBeenSentBefore);

                // execute the command
                _conn.Open();

                try
                {
                    _cmd.ExecuteNonQuery();
                    _hasFileBeenSentBefore = (bool)_cmd.Parameters["@FileHasBeenSentBefore"].Value;
                }
                catch (Exception e)
                {
                    Trace.TraceError("[" + _nameIdentifier + "] DB-Call: Could not execute DB-check! -> " + e.ToString());
                    throw;
                }
                finally
                {
                    _conn.Close();
                }

                Trace.TraceInformation("[" + _nameIdentifier + "] DB-Call: Has file been sent before: " + _hasFileBeenSentBefore.ToString());

                return _hasFileBeenSentBefore;

            }

        }

        public static void Logfile(string fileName, string status, string comment)
        {
            var _connectionStringEntry = ConfigurationManager.ConnectionStrings["cramo-esb-db"];

            string _connectionString = _connectionStringEntry.ConnectionString;

            using (SqlConnection _conn = new SqlConnection(_connectionString))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand _cmd = new SqlCommand("sp_InsertFilelogRecord", _conn);

                // 2. set the command object so it knows to execute a stored procedure
                _cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                _cmd.Parameters.Add(new SqlParameter("@Filename", fileName));
                _cmd.Parameters.Add(new SqlParameter("@Status", status));
                _cmd.Parameters.Add(new SqlParameter("@Comment", comment));

                // execute the command
                _conn.Open();

                try
                {
                    _cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    _conn.Close();
                }

            }
        }

        private static Dictionary<string, string> GetPurchaseOrderInfo(string content)
        {

            Trace.TraceInformation("[" + _nameIdentifier + "] Getting order id info.");
            Dictionary<string, string> orderInfo = new Dictionary<string, string>();

            try
            {
                XDocument xdoc = XDocument.Parse(content);

                string orderId = xdoc.Element("Order").Element("OrderNumber").Value;
                Trace.TraceInformation("[" + _nameIdentifier + "] Order number found: " + orderId);
                orderInfo.Add("OrderId", orderId);
            }
            catch (Exception e) {
                Trace.TraceWarning("[" + _nameIdentifier + "] Failed to get PO number info: " + e.Message.ToString());
            }

            return orderInfo;

        }

        public String createHTTPTopicPost(String xmlContent)
        {
            String responseBody = "";

            string baseAddress = System.Configuration.ConfigurationManager.AppSettings["Topic.BaseAddress"];
            string uri = System.Configuration.ConfigurationManager.AppSettings["Topic.PO.Uri"];
            string subscriptionkey = System.Configuration.ConfigurationManager.AppSettings["Topic.SubscriptionKey"];

            String completeUrl = baseAddress + uri;
            int iTimeOutInMs = 15000;

            HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(completeUrl);
            myHttpWebRequest.Timeout = iTimeOutInMs;
            myHttpWebRequest.ReadWriteTimeout = iTimeOutInMs;
            myHttpWebRequest.KeepAlive = false;
            myHttpWebRequest.Method = "POST";
            myHttpWebRequest.Headers.Set("Ocp-Apim-Subscription-Key", subscriptionkey);

            Trace.TraceInformation("[" + _nameIdentifier + "] Sending message to: " + completeUrl);

            byte[] data = Encoding.UTF8.GetBytes(xmlContent);

            myHttpWebRequest.ContentType = "text/xml";
            myHttpWebRequest.ContentLength = data.Length;

            Stream requestStream = myHttpWebRequest.GetRequestStream();
            requestStream.Write(data, 0, data.Length);
            requestStream.Close();

            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            HttpStatusCode sc = myHttpWebResponse.StatusCode;

            Stream responseStream = myHttpWebResponse.GetResponseStream();

            StreamReader myStreamReader = new StreamReader(responseStream, Encoding.UTF8);
            responseBody = myStreamReader.ReadToEnd();


            // Clean up
            myStreamReader.Close();
            responseStream.Close();
            myHttpWebResponse.Close();

            Trace.TraceInformation("[" + _nameIdentifier + "] HTTP Post response: " + sc.ToString() + ", Body: " + responseBody.ToString());
            return sc.ToString();
        }

        private static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Trace.TraceError(e.ExceptionObject.ToString());
        }
    }

 }
